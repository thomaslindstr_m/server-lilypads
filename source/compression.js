const {compress} = require('iltorb');

const objectHasProperty = require('@amphibian/object-has-property');
const errors = require('@amphibian/errors');
const parties = require('@amphibian/party');
const createCache = require('@amphibian/cache');

const compressionCache = createCache({id: 'lilypads-compression'});
const compressors = {};

function compression(id, options = {}) {
    if (objectHasProperty(compressors, id) && compressors[id]) {
        return compressors[id];
    }

    const cache = compressionCache.open(id);
    let timeout = null;

    const compressor = {
        encoding: () => 'br',
        ready: () => cache.fresh() || false,
        party: parties(id),

        output() {
            if (this.party.exists()) {
                return this.party.crash();
            } else if (this.ready()) {
                this.decay();
                return Promise.resolve(cache.get());
            }

            return Promise.reject(errors.fatalError('no_compressed_input', id));
        },
        async input(input) {
            if (this.party.exists()) {
                await this.party.crash();
            }

            return this.party.host(new Promise((resolve, reject) => {
                clearTimeout(timeout);

                compress(Buffer.from(input), (error, output) => {
                    if (error) {
                        reject(error);
                        return;
                    }

                    this.decay();
                    cache.set(output);
                    resolve(output);
                });
            }));
        },
        remove() {
            try {
                cache.invalidate();
            } catch (error) {
                // ... do nothing
            }

            clearTimeout(timeout);
            timeout = null;
            compressors[id] = null;
            delete compressors[id];
        },
        decay() {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(() => {
                this.remove();
            }, options.lifetime || 5 * 60 * 1000);
        }
    };

    compressors[id] = compressor;
    return compressor;
}

module.exports = {compression, compressors};
