const isNumber = require('@amphibian/is-number');
const isString = require('@amphibian/is-string');
const isObject = require('@amphibian/is-object');
const inArray = require('@amphibian/in-array');
const createCache = require('@amphibian/cache');
const parties = require('@amphibian/party');
const errors = require('@amphibian/errors');

const {compression} = require('./compression');

const CACHE_LIFETIME = 6 * 60 * 60 * 1000; // 6 hours
const leapCache = createCache();

/**
 * Leap
 * Always return the result of `responder` immediately, if cached
 * Return compressed version if compressor is done
 *
 * @param {object} options
 * @param {function} responder
 * @param {function} errorHandler
 *
 * @returns {promise}
**/
function leap(options, responder, errorHandler) {
    return new Promise(async (resolve, reject) => {
        try {
            const {id, encodings, lifetime} = options;
            const cache = leapCache.open(id);
            const compressor = compression(id, {lifetime});
            const forceUpdate = Boolean(options.forceUpdate);
            const synchronous = options.forceUpdate === 'sync';

            const lilypad = {
                timestamp: null,
                headers: {},
                body: null,
                original: null,
                isResolved: false,
                isCached: false,
                isCompressed: false,
                getCompressedOutput: () => compressor.output()
            };

            if (!synchronous && cache.fresh()) {
                const cacheContents = cache.get();

                lilypad.timestamp = cacheContents.timestamp;
                lilypad.headers = Object.assign({}, cacheContents.headers);
                lilypad.original = cacheContents.response;
                lilypad.isCached = true;

                if (encodings && inArray(compressor.encoding(), encodings)) {
                    if (compressor.ready()) {
                        lilypad.body = await compressor.output();
                        lilypad.headers['content-encoding'] = compressor.encoding();
                        lilypad.isResolved = true;
                        lilypad.isCompressed = true;

                        resolve(lilypad);
                    } else if (!compressor.party.exists()) {
                        const {original} = lilypad;

                        if (isString(original)) {
                            compressor.input(original);
                        } else if (isObject(original)) {
                            compressor.input(JSON.stringify(original));
                        }
                    }
                }

                if (!lilypad.isResolved) {
                    lilypad.body = lilypad.original;
                    lilypad.isResolved = true;

                    resolve(lilypad);
                }

                if (!forceUpdate) {
                    if (isNumber(lifetime)) {
                        if ((Date.now() - cacheContents.timestamp) < lifetime) {
                            return; // Cache is still fresh
                        }
                    } else {
                        return; // Assume Infinity lifetime
                    }
                }
            }

            const party = parties(id);

            if (party.exists()) {
                if (forceUpdate) {
                    await party.crash();
                } else {
                    resolve(party.crash());
                    return;
                }
            }

            const host = party.host(
                new Promise(async (resolveParty, rejectParty) => {
                    try {
                        const response = await responder();
                        const creationTimestamp = Date.now();
                        const headers = {
                            'last-modified': new Date(creationTimestamp).toGMTString()
                        };

                        if (options.encodings) {
                            if (isString(response)) {
                                compressor.input(response);
                                headers['content-type'] = 'text/plain; charset=utf-8';
                            } else if (isObject(response)) {
                                compressor.input(JSON.stringify(response));
                                headers['content-type'] = 'application/json; charset=utf-8';
                            } else {
                                throw errors.fatalError('unhandled_response_type', response);
                            }
                        }

                        cache.set({
                            timestamp: creationTimestamp,
                            headers,
                            response
                        }, {lifetime: CACHE_LIFETIME});

                        lilypad.timestamp = creationTimestamp;
                        lilypad.headers = headers;
                        lilypad.body = response;
                        lilypad.original = response;
                        lilypad.isResolved = true;
                    } catch (error) {
                        if (errorHandler) {
                            errorHandler(error);
                        }

                        if (synchronous && cache.fresh()) {
                            delete options.forceUpdate;
                            resolveParty(leap(options, responder, errorHandler));
                            return;
                        }

                        if (!lilypad.isResolved) {
                            rejectParty(error);
                        }
                    }

                    resolveParty(lilypad);
                })
            );

            if (!lilypad.isResolved) {
                return resolve(host);
            }
        } catch (error) {
            reject(error);
        }
    });
}

module.exports = leap;
