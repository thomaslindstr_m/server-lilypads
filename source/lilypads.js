const leap = require('./leap');

/**
 * Lilypads
 * Handles and sends the server response
 *
 * @param {object} context
 * @param {object} options
 * @param {function} handler
 * @param {function} errorHandler
 *
 * @returns {promise}
**/
async function lilypads(context, options, handler, errorHandler) {
    context.status = 200;

    const lilypad = await leap({
        id: options.id,
        encodings: (options.disableCompression === true)
            ? []
            : context.get('accept-encoding').split(/,\s?/),
        lifetime: options.lifetime,
        forceUpdate: options.forceUpdate
    }, handler, errorHandler);

    context.set(lilypad.headers);

    if (context.fresh) {
        context.status = 304;
        return;
    }

    context.body = lilypad.body;
}

module.exports = lilypads;
