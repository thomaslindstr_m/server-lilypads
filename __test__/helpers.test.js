//  --------------------------------------------------------------------------
//  __test__: helpers.test.js
//  --------------------------------------------------------------------------

const isString = require('@amphibian/is-string');
const {expectToThrow, randomString, delay} = require('./helpers');

test('callback with error on test failure', () => {
    const errorMessage = 'Some error';

    return expectToThrow(() => {
        throw new Error(errorMessage);
    }, (error) => {
        expect(error.message).toBe(errorMessage);
    });
});

test('failing: throw when no errors are thrown', async () => {
    try {
        await expectToThrow(() => {});
    } catch (error) {
        expect(error.message).toBe('No error thrown.');
        return;
    }

    throw new Error('No error thrown.');
});

test('get a random string', () => {
    expect(isString(randomString())).toBe(true);
});

test('wait X milliseconds', () => (
    delay(250)
));
