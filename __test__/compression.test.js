//  --------------------------------------------------------------------------
//  __test__: compression.test.js
//  --------------------------------------------------------------------------

const {decompress} = require('iltorb');
const pify = require('pify');
const objectHasProperty = require('@amphibian/object-has-property');
const {compression, compressors} = require('../source/compression');
const {expectToThrow, randomString, delay} = require('./helpers');

test('should provide existing compressor if present', () => {
    const id = randomString();
    compression(id);
    compression(id);
});

test('should not be initially ready', () => {
    const compressor = compression(randomString());
    expect(compressor.ready()).toBe(false);
});

test('should have brotli encoding', () => {
    const compressor = compression(randomString());
    expect(compressor.encoding()).toBe('br');
});

test('should fail when initially getting output', () => {
    const id = randomString();

    return expectToThrow(async () => {
        const compressor = compression(id);
        await compressor.output();
    }, (error) => {
        expect(error.code).toBe('no_compressed_input');
        expect(error.data[0]).toBe(id);
    });
});

test('should be able to remove compressed', () => {
    const id = randomString();
    const compressor = compression(id);

    expect(objectHasProperty(compressors, id)).toBe(true);
    compressor.remove();
    expect(objectHasProperty(compressors, id)).toBe(false);
});

test('should be able to recreate compressor', () => {
    const id = randomString();
    const compressor = compression(id);

    expect(objectHasProperty(compressors, id)).toBe(true);
    compressor.remove();
    expect(objectHasProperty(compressors, id)).toBe(false);

    compression(id);
});

test('should be able to decay', () => {
    const compressor = compression(randomString());
    compressor.decay();
});

test('should be able to compress input', () => {
    const compressor = compression(randomString());
    return compressor.input('test');
});

test('should be ready after compressing input', async () => {
    const compressor = compression(randomString());
    await compressor.input('test');
    expect(compressor.ready()).toBe(true);
});

test('should get output after compressing input', async () => {
    const compressor = compression(randomString());
    const input = 'test';

    await compressor.input(input);
    const output = await compressor.output();

    return pify(decompress)(output).then((decompressed) => {
        expect(Buffer.from(decompressed).toString()).toBe(input);
    });
});

test('should be able to get output while compressing input', async () => {
    const compressor = compression(randomString());
    const input = 'test';

    compressor.input(input);
    const output = await compressor.output();

    return pify(decompress)(output).then((decompressed) => {
        expect(Buffer.from(decompressed).toString()).toBe(input);
    });
});

test('should sequentially compress input', async () => {
    const compressor = compression(randomString());
    const input1 = 'test';
    const input2 = 'test2';

    compressor.input(input1);
    compressor.input(input2);

    const output1 = await compressor.output();

    await pify(decompress)(output1).then((decompressed) => {
        expect(Buffer.from(decompressed).toString()).toBe(input1);
    });

    const output2 = await compressor.output();

    await pify(decompress)(output2).then((decompressed) => {
        expect(Buffer.from(decompressed).toString()).toBe(input2);
    });
});

test('should remove itself when decayed', async () => {
    const id = randomString();
    const lifetime = 500;
    const compressor = compression(id, {lifetime});
    await compressor.input('test');

    expect(objectHasProperty(compressors, id)).toBe(true);
    expect((await compressor.ready())).toBe(true);

    await delay(lifetime);

    expect(objectHasProperty(compressors, id)).toBe(false);
    expect((await compressor.ready())).toBe(false);
});
