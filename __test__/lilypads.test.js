//  --------------------------------------------------------------------------
//  __test__: lilypads.test.js
//  --------------------------------------------------------------------------

const {randomString} = require('./helpers');
const lilypads = require('../source/lilypads');
const leap = require('../source/leap');

test('should set context.status', async () => {
    const context = {
        headers: {},
        get: () => '',
        set(headers) {
            this.headers = headers;
        }
    };

    await lilypads(context, {id: randomString()}, () => 'piss');
    expect(context.status).toBe(200);
});

test('should set not-modified context.status if fresh', async () => {
    const context = {
        fresh: true,
        headers: {},
        get: () => '',
        set(headers) {
            this.headers = headers;
        }
    };

    await lilypads(context, {id: randomString()}, () => 'piss');
    expect(context.status).toBe(304);
});

test('should set context content-type header', async () => {
    const context = {
        headers: {},
        get: () => '',
        set(headers) {
            this.headers = headers;
        }
    };

    await lilypads(context, {id: randomString()}, () => 'piss');
    expect(context.headers['content-type']).toBe('text/plain; charset=utf-8');
});

test('should set context last-modified header', async () => {
    const context = {
        headers: {},
        get: () => '',
        set(headers) {
            this.headers = headers;
        }
    };

    await lilypads(context, {id: randomString()}, () => 'piss');
    expect(Boolean(context.headers['last-modified'])).toBe(true);
});

test('should set context.body', async () => {
    const context = {
        headers: {},
        get: () => '',
        set(headers) {
            this.headers = headers;
        }
    };

    const body = 'piss';
    await lilypads(context, {id: randomString()}, () => body);
    expect(context.body).toBe(body);
});

test('should set compression headers', async () => {
    const context = {
        headers: {},
        get: () => 'br',
        set(headers) {
            this.headers = headers;
        }
    };

    const id = randomString();
    const body = 'piss';

    await lilypads(context, {id}, () => body);
    expect(context.body).toBe(body);
    const lilypad = await leap({id}, () => body);

    await lilypad.getCompressedOutput();
    await lilypads(context, {id}, () => body);

    expect(context.headers['content-encoding']).toBe('br');
});

test('should be able to disable compression', async () => {
    const context = {
        headers: {},
        get: () => 'br',
        set(headers) {
            this.headers = headers;
        }
    };

    const id = randomString();
    const body = 'piss';

    await lilypads(context, {id, disableCompression: true}, () => body);
    expect(context.body).toBe(body);
    const lilypad = await leap({id}, () => body);

    await lilypad.getCompressedOutput();
    await lilypads(context, {id, disableCompression: true}, () => body);

    expect(context.headers['content-encoding']).toBe(undefined);
});
