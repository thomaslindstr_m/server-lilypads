//  --------------------------------------------------------------------------
//  __test__: leap.test.js
//  --------------------------------------------------------------------------

const {decompress} = require('iltorb');
const pify = require('pify');
const {expectToThrow, randomString, delay} = require('./helpers');
const leap = require('../source/leap');

test('should return lilypad (if any) when responder is not provided', async () => {
    const id = randomString();
    const content = 'hello';

    await leap({id}, () => content);

    const lilypad = await leap({id});
    expect(lilypad.original).toBe(content);
});

test('should throw faulty responder', () => (
    expectToThrow(() => (
        leap({id: randomString()}, () => {
            throw new Error('test');
        })
    ), (error) => {
        expect(error.message).toBe('test');
    })
));

test('should update responder values', async () => {
    const id = randomString();
    const lifetime = 0;
    const content = 'test';

    await leap({id, lifetime}, () => 'something');
    await leap({id, lifetime}, () => content);

    const lilypad = await leap({id, lifetime});

    expect(lilypad.original).toBe(content);
});

test('should not throw faulty responder if resolved by previous responder', async () => {
    const id = randomString();
    const content = 'test';

    await leap({id}, () => content);

    const lilypad = await leap({id}, () => {
        throw new Error('test');
    });

    expect(lilypad.original).toBe(content);
});

test('should not throw faulty responder after exceeded lifetime if resolved by previous responder', async () => {
    const id = randomString();
    const content = 'test';
    const options = {id, lifetime: 500};

    await leap(options, () => content);
    await delay(options.lifetime + 100);

    const lilypad = await leap(options, () => {
        throw new Error('test');
    });

    expect(lilypad.original).toBe(content);
});

test('should gracefully use the most recent successful responder result', async () => {
    const id = randomString();
    const content = 'hello';

    await leap({id}, () => content);

    const lilypad = await leap({id}, async () => {
        throw new Error('test');
    });

    expect(lilypad.original).toBe(content);
});

test('should gracefully use the most recent successful responder result when it changed post-error', async () => {
    const id = randomString();
    const lifetime = 0;
    const content = 'hello';

    await leap({id, lifetime}, () => 'not the variable');
    await leap({id, lifetime}, async () => {
        throw new Error('test');
    });

    await delay(0);
    await leap({id, lifetime}, () => content);
    const lilypad = await leap({id, lifetime}, () => content);

    expect(lilypad.original).toBe(content);
});

test('should not call responder multiple times', async () => {
    const id = randomString();
    let counter = 0;

    await leap({id}, () => {
        counter += 1;
        return 'test';
    });

    await leap({id}, () => {
        counter += 1;
        return 'test';
    });

    await leap({id}, () => {
        counter += 1;
        return 'test';
    });

    expect(counter).toBe(1);
});

test('should throw when errorHandler is provided and there is no previous responder, but also call errorHandler, first', async () => {
    const id = randomString();
    const lifetime = 0;
    let callResult = '';

    try {
        await leap({id, lifetime}, () => {
            throw new Error('test');
        }, (error) => {
            callResult += 'a';
            expect(error.message).toBe('test');
        });
    } catch (error) {
        callResult += 'b';
        expect(error.message).toBe('test');
    }

    expect(callResult).toBe('ab');
});

test('should report errors to errorHandler function', async () => {
    const id = randomString();
    const lifetime = 0;

    let isCalled = false;

    await leap({id, lifetime}, () => 'content');
    await leap({id, lifetime}, () => {
        throw new Error('test');
    }, (error) => {
        isCalled = true;
        expect(error.message).toBe('test');
    });

    expect(isCalled).toBe(true);
});

test('should get lilypad body', async () => {
    const body = 'test';
    const lilypad = await leap({id: randomString()}, () => body);
    expect(lilypad.body).toBe(body);
});

test('should get initial lilypad body directly', async () => {
    const body = 'test';
    const lilypad = await leap({id: randomString()}, () => body);
    expect(lilypad.isCached).toBe(false);
});

test('should get initial lilypad body uncompressed', async () => {
    const body = 'test';
    const lilypad = await leap({id: randomString()}, () => body);
    expect(lilypad.isCompressed).toBe(false);
});

test('should get compressed lilypad body', async () => {
    const id = randomString();

    const firstLilypad = await leap({id, encodings: ['br']}, () => 'test');
    await firstLilypad.getCompressedOutput();
    const secondLilypad = await leap({id, encodings: ['br']}, () => 'test');

    expect(firstLilypad.isCompressed).toBe(false);
    expect(secondLilypad.isCompressed).toBe(true);
});

test('should get compressed lilypad body from cache', async () => {
    const id = randomString();

    const firstLilypad = await leap({id, encodings: ['br']}, () => 'test');
    await firstLilypad.getCompressedOutput();
    const secondLilypad = await leap({id, encodings: ['br']}, () => 'test');

    expect(firstLilypad.isCached).toBe(false);
    expect(secondLilypad.isCached).toBe(true);
});

test('should get second lilypad body from cache', async () => {
    const id = randomString();
    const body = 'test';

    const firstLilypad = await leap({id}, () => body);
    const secondLilypad = await leap({id}, () => body);

    expect(firstLilypad.isCached).toBe(false);
    expect(secondLilypad.isCached).toBe(true);
});

test('should get lilypad string body', async () => {
    const body = 'test';
    const lilypad = await leap({id: randomString()}, () => body);
    expect(lilypad.body).toBe(body);
});

test('should get lilypad text header when compressed', async () => {
    const lilypad = await leap({
        id: randomString(),
        encodings: ['br']
    }, () => 'test');

    await lilypad.getCompressedOutput();
    expect(lilypad.headers['content-type']).toBe('text/plain; charset=utf-8');
});

test('should get lilypad compressed string body', async () => {
    const id = randomString();
    const body = 'test';
    const firstLilypad = await leap({id, encodings: ['br']}, () => body);
    await firstLilypad.getCompressedOutput();
    const secondLilypad = await leap({id, encodings: ['br']}, () => body);

    await pify(decompress)(secondLilypad.body).then((decompressed) => {
        expect(Buffer.from(decompressed).toString()).toBe(body);
    });

    expect(secondLilypad.headers['content-type']).toBe('text/plain; charset=utf-8');
    expect(secondLilypad.headers['content-encoding']).toBe('br');
});

test('should get lilypad compressed text body with later provided compression', async () => {
    const id = randomString();
    const firstLilypad = await leap({id}, () => 'test');

    expect(firstLilypad.body).toBe('test');
    expect(firstLilypad.original).toBe('test');
    expect(firstLilypad.isCompressed).toBe(false);

    const secondLilypad = await leap({id, encodings: ['br']}, () => 'test');
    await secondLilypad.getCompressedOutput();

    const thirdLilypad = await leap({id, encodings: ['br']}, () => 'test');
    await thirdLilypad.getCompressedOutput();

    expect(thirdLilypad.isCompressed).toBe(true);
});

test('should get lilypad json body', async () => {
    const lilypad = await leap({id: randomString()}, () => ({
        test: true
    }));

    expect(lilypad.body.test).toBe(true);
});

test('should get lilypad json header when compressed', async () => {
    const lilypad = await leap({
        id: randomString(),
        encodings: ['br']
    }, () => ({test: true}));

    await lilypad.getCompressedOutput();
    expect(lilypad.headers['content-type']).toBe('application/json; charset=utf-8');
});

test('should get lilypad compressed json body', async () => {
    const id = randomString();
    const firstLilypad = await leap({id, encodings: ['br']}, () => ({
        test: true
    }));

    await firstLilypad.getCompressedOutput();

    const secondLilypad = await leap({id, encodings: ['br']}, () => ({
        test: true
    }));

    await pify(decompress)(secondLilypad.body).then((decompressed) => {
        expect(
            JSON.parse(Buffer.from(decompressed).toString()).test
        ).toBe(true);
    });

    expect(secondLilypad.headers['content-type']).toBe('application/json; charset=utf-8');
    expect(secondLilypad.headers['content-encoding']).toBe('br');
});

test('should get lilypad compressed json body with later provided compression', async () => {
    const id = randomString();
    const firstLilypad = await leap({id}, () => ({test: true}));

    expect(firstLilypad.body.test).toBe(true);
    expect(firstLilypad.original.test).toBe(true);
    expect(firstLilypad.isCompressed).toBe(false);

    const secondLilypad = await leap({id, encodings: ['br']}, () => ({test: true}));
    await secondLilypad.getCompressedOutput();

    const thirdLilypad = await leap({id, encodings: ['br']}, () => ({test: true}));
    await thirdLilypad.getCompressedOutput();

    expect(thirdLilypad.isCompressed).toBe(true);
});

test('should throw on unhandled response type when compressing', async () => (
    expectToThrow(() => (
        leap({id: randomString(), encodings: ['br']}, () => [])
    ), (error) => {
        expect(error.code).toBe('unhandled_response_type');
    })
));

test('should setup lilypad with lifetime', async () => (
    leap({id: randomString(), lifetime: 1000}, () => 'test')
));

test('should avoid requesting new response when alive', async () => {
    const id = randomString();

    const firstLilypad = await leap({id, lifetime: 1000}, () => 'test');
    const secondLilypad = await leap({id, lifetime: 1000}, () => 'test');

    expect(firstLilypad.isCached).toBe(false);
    expect(secondLilypad.isCached).toBe(true);
});

test('should call responder once when alive', async () => {
    const id = randomString();
    let counter = 0;

    await leap({id, lifetime: 1000}, () => {
        counter += 1;
        return 'test';
    });

    await leap({id, lifetime: 1000}, () => {
        counter += 1;
        return 'test';
    });

    await leap({id, lifetime: 1000}, () => {
        counter += 1;
        return 'test';
    });

    expect(counter).toBe(1);
});

test('should not call responder multiple times when stale and concurrent', async () => {
    const id = randomString();
    let counter = 0;

    await Promise.all([
        leap({id, lifetime: -1}, () => {
            counter += 1;
            return 'test';
        }),
        leap({id, lifetime: -1}, () => {
            counter += 1;
            return 'test';
        }),
        leap({id, lifetime: -1}, () => {
            counter += 1;
            return 'test';
        })
    ]);

    expect(counter).toBe(1);
});

test('should call responder multiple times when stale and not concurrent', async () => {
    const id = randomString();
    let counter = 0;

    await leap({id, lifetime: -1}, () => {
        counter += 1;
        return 'test';
    });

    await delay(500);

    await leap({id, lifetime: -1}, () => {
        counter += 1;
        return 'test';
    });

    await delay(500);

    await leap({id, lifetime: -1}, () => {
        counter += 1;
        return 'test';
    });

    expect(counter).toBe(3);
});

test('should re-input string to decayed compressor upon new request', async () => {
    const id = randomString();
    const lifetime = 500;
    const leapOptions = {id, lifetime, encodings: ['br']};
    const lilypadBody = 'test';

    await leap(leapOptions, () => lilypadBody);
    await delay(lifetime + 100);

    const firstLilypad = await leap(leapOptions, () => lilypadBody);
    expect(firstLilypad.isCompressed).toBe(false);

    await firstLilypad.getCompressedOutput();

    const secondLilypad = await leap(leapOptions, () => lilypadBody);
    expect(secondLilypad.isCompressed).toBe(true);
});

test('should re-input object to decayed compressor upon new request', async () => {
    const id = randomString();
    const lifetime = 500;
    const leapOptions = {id, lifetime, encodings: ['br']};
    const lilypadBody = {test: true};

    await leap(leapOptions, () => lilypadBody);
    await delay(lifetime + 100);

    const firstLilypad = await leap(leapOptions, () => lilypadBody);
    expect(firstLilypad.isCompressed).toBe(false);

    await firstLilypad.getCompressedOutput();

    const secondLilypad = await leap(leapOptions, () => lilypadBody);
    expect(secondLilypad.isCompressed).toBe(true);
});

test('should not re-input multiple times on concurrent requests', async () => {
    const id = randomString();
    const lifetime = 500;
    const leapOptions = {id, lifetime, encodings: ['br']};
    const lilypadBody = 'test';

    await leap(leapOptions, () => lilypadBody);
    await delay(lifetime + 100);

    const firstLilypad = await leap(leapOptions, () => lilypadBody);
    expect(firstLilypad.isCompressed).toBe(false);

    await leap(leapOptions, () => lilypadBody);
    await firstLilypad.getCompressedOutput();

    const secondLilypad = await leap(leapOptions, () => lilypadBody);
    expect(secondLilypad.isCompressed).toBe(true);
});

test('should force update the responder when forceUpdate is async', async () => {
    const id = randomString();
    const lifetime = 500;
    const body = 'hello';
    const finalForm = 'there';

    const firstLilypad = await leap({id, lifetime}, () => body);
    expect(firstLilypad.original).toBe(body);

    const secondLilypad = await leap({id, lifetime}, () => `${body}2`);
    expect(secondLilypad.original).toBe(body);

    const thirdLilypad = await leap({id, lifetime, forceUpdate: 'async'}, async () => {
        await delay(250);
        return finalForm;
    });

    expect(thirdLilypad.original).toBe(body);
    await delay(500);

    const fourthLilypad = await leap({id, lifetime}, () => 'nothing');
    expect(fourthLilypad.original).toBe(finalForm);
});

test('should asynchronously force update the responder when forceUpdate is async', async () => {
    const id = randomString();
    const lifetime = 2000;
    const body = 'hello';
    const finalForm = 'there';

    const firstLilypad = await leap({id, lifetime}, () => body);
    expect(firstLilypad.original).toBe(body);

    const secondLilypad = await leap({id, lifetime}, () => `${body}2`);
    expect(secondLilypad.original).toBe(body);

    const thirdLilypad = await leap({id, lifetime, forceUpdate: 'async'}, async () => {
        await delay(250);
        return finalForm;
    });

    expect(thirdLilypad.original).toBe(body);
    await delay(500);

    const fourthLilypad = await leap({id, lifetime});
    expect(fourthLilypad.original).toBe(finalForm);
});

test('should handle errors during asynchronous updates', async () => {
    const id = randomString();
    const lifetime = 2000;
    const body = 'hello';

    const firstLilypad = await leap({id, lifetime}, () => body);
    expect(firstLilypad.original).toBe(body);

    const secondLilypad = await leap({id, lifetime, forceUpdate: 'async'}, () => {
        throw new Error('test');
    });

    expect(secondLilypad.original).toBe(body);

    const thirdLilypad = await leap({id, lifetime}, () => `${body}2`);
    expect(thirdLilypad.original).toBe(body);
});

test('should force update when trying to async forceUpdate more than once simultaneously', async () => {
    const id = randomString();
    const lifetime = 2000;
    const body = 'hello';

    const firstLilypad = await leap({id, lifetime}, () => body);
    expect(firstLilypad.original).toBe(body);

    await Promise.all([
        leap({id, lifetime, forceUpdate: 'async'}, async () => {
            await delay(250);
            return `${body}2`;
        }),
        leap({id, lifetime, forceUpdate: 'async'}, async () => {
            await delay(500);
            return `${body}3`;
        })
    ]);

    const secondLilypad = await leap({id, lifetime});
    expect(secondLilypad.original).toBe(body);

    await delay(300);

    const thirdLilypad = await leap({id, lifetime});
    expect(thirdLilypad.original).toBe(`${body}2`);

    await delay(550);

    const fourthLilypad = await leap({id, lifetime});
    expect(fourthLilypad.original).toBe(`${body}3`);
});

test('should force update the responder when forceUpdate is sync', async () => {
    const id = randomString();
    const lifetime = 500;
    const body = 'hello';
    const finalForm = 'there';

    const firstLilypad = await leap({id, lifetime}, () => body);
    expect(firstLilypad.original).toBe(body);

    const secondLilypad = await leap({id, lifetime}, () => `${body}2`);
    expect(secondLilypad.original).toBe(body);

    const thirdLilypad = await leap({id, lifetime, forceUpdate: 'sync'}, async () => {
        await delay(250);
        return finalForm;
    });

    expect(thirdLilypad.original).toBe(finalForm);
});

test('should fallback to previous responder result if force updating the responder fails during sync', async () => {
    const id = randomString();
    const lifetime = 500;
    const body = 'hello';
    let tried = false;

    const firstLilypad = await leap({id, lifetime}, () => body);
    expect(firstLilypad.original).toBe(body);

    const secondLilypad = await leap({id, lifetime, forceUpdate: 'sync'}, async () => {
        await delay(250);
        tried = true;
        throw new Error('test');
    });

    expect(secondLilypad.original).toBe(body);
    expect(tried).toBe(true);
});

test('should throw when forceUpdate is synchronous and no previous responder was resolved', () => (
    expectToThrow(() => (
        leap({id: randomString(), forceUpdate: 'sync'}, () => {
            throw new Error('test');
        })
    ), (error) => {
        expect(error.message).toBe('test');
    })
));
