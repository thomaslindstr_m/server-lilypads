//  --------------------------------------------------------------------------
//  __test__: helpers.js
//  --------------------------------------------------------------------------

/**
 * Expect a function to throw
 * @param {function} test
 * @param {function} callback
**/
async function expectToThrow(test, callback) {
    try {
        await test();
    } catch (error) {
        callback(error);
        return;
    }

    throw new Error('No error thrown.');
}

/**
 * Generate a random string
 * @returns {string}
**/
function randomString() {
    return Math.random().toString(36).substring(2);
}

/**
 * Delay X milliseconds
 * @returns {promise}
**/
function delay(milliseconds) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

module.exports = {
    expectToThrow,
    randomString,
    delay
};
